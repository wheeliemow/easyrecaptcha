﻿namespace EasyReCaptcha
{
    public static class Constants
    {
        public static string SiteKey { get { return "Recaptcha_SiteKey"; } }

        public static string SecretKey { get { return "Recaptcha_Secret"; } }

        public static string BaseUrl { get { return "https://www.google.com/recaptcha/api"; } }

        public static string UseRemoteIp { get { return "Recaptcha_UseRemoteIP"; } }
    }
}