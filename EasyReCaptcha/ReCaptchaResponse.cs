﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EasyReCaptcha
{
    public class ReCaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error-codes")]
        public IList<string> ErrorCodes { get; set; }
    }
}