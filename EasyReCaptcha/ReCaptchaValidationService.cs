﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;

namespace EasyReCaptcha
{
    public class ReCaptchaValidationService
    {
        // private const string BaseUrl = "https://www.google.com/recaptcha/api/siteverify";

        public string Secret
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.SecretKey]))
                {
                    throw new Exception("Configuration not set");
                }
                return ConfigurationManager.AppSettings[Constants.SecretKey];
            }
        }

        public bool UseRemoteIP
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.UseRemoteIp]))
                {
                    throw new Exception("Configuration Error");
                }

                return Convert.ToBoolean(ConfigurationManager.AppSettings[Constants.UseRemoteIp]);
            }
        }

        public ReCaptchaValidationService()
        {
        }

        public ReCaptchaResponse Validate(string response, string remoteIp)
        {
            var url = new StringBuilder(string.Format("{0}/siteverify?secret={1}&response={2}", Constants.BaseUrl, Secret, response));

            if (UseRemoteIP && !string.IsNullOrEmpty(remoteIp))
            {
                url.Append(string.Format("&remoteid={0}", remoteIp));
            }
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var hrm = httpClient.GetAsync(url.ToString()).Result;
                    hrm.EnsureSuccessStatusCode();
                    var result = hrm.Content.ReadAsStringAsync().Result;

                    return JsonConvert.DeserializeObject<ReCaptchaResponse>(result);
                }
            }
            catch (Exception)
            {
                //TODO need to finish this
            }

            return new ReCaptchaResponse() { Success = false };
        }
    }
}