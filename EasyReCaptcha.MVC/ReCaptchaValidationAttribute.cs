﻿using EasyReCaptcha.Resources;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EasyReCaptcha.MVC
{
    public class ReCaptchaValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var response = filterContext.HttpContext.Request.Form["g-recaptcha-response"];
            var remoteIP = filterContext.HttpContext.Request.UserHostAddress;

            var rvs = new ReCaptchaValidationService();

            var result = rvs.Validate(response, remoteIP);

            if (!result.Success)
            {
                filterContext.Controller.ViewData.ModelState.AddModelError("ReCaptcha_Errors", ErrorCodes.ResponseError_ValidationFailed);

                var errors = new List<string>();

                foreach (var error in result.ErrorCodes)
                {
                    switch (error)
                    {
                        case ("missing-input-secret"):
                            errors.Add(ErrorCodes.ResponseError_MissingInputSecret);
                            break;

                        case ("invalid-input-secret"):
                            errors.Add(ErrorCodes.ResponseError_InvalidInputSecret);
                            break;

                        case ("missing-input-response"):
                            errors.Add(ErrorCodes.ResponseError_MissingInputResponse);
                            break;

                        case ("invalid-input-response"):
                            errors.Add(ErrorCodes.ResponseError_InvalidInputResponse);
                            break;

                        default:
                            errors.Add(ErrorCodes.ResponseError_GeneralError);
                            break;
                    }
                }

                filterContext.HttpContext.Items["ReCaptcha.Errors"] = errors;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}