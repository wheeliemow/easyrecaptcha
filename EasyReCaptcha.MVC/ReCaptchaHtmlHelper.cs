﻿using System;
using System.Configuration;
using System.Text;
using System.Web.Mvc;

namespace EasyReCaptcha.MVC
{
    public static class ReCaptchaHtmlHelper
    {
        public static MvcHtmlString ReCaptcha(this HtmlHelper helper, string language)
        {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.SiteKey]))
            {
                throw new Exception("Missing configuration information");
            }

            var script = new TagBuilder("script");

            var src = new StringBuilder(string.Format("{0}", Constants.BaseUrl));
            if (string.IsNullOrEmpty(language))
            {
                src.Append(".js");
            }
            else
            {
                src.Append(".js?hl=");
                src.Append(language);
            }

            script.MergeAttribute("src", src.ToString());
            script.MergeAttribute("async", "true");
            script.MergeAttribute("defer", "true");

            var div = new TagBuilder("div");
            div.MergeAttribute("class", "g-recaptcha");
            div.MergeAttribute("data-sitekey", ConfigurationManager.AppSettings[Constants.SiteKey]);

            return new MvcHtmlString(div.ToString(TagRenderMode.Normal) + "\r\n" + script.ToString(TagRenderMode.Normal));
        }
    }
}