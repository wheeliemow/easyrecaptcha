﻿using System;

namespace EasyReCaptcha.WebForms
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {


        }

        public bool Validate()
        {

            var response = Request.Form["g-recaptcha-response"];
            var remoteIP = Request.UserHostAddress;


            var rvs = new ReCaptchaValidationService();
            var result = rvs.Validate(response, remoteIP);



            return result.Success;



        }
    }
}