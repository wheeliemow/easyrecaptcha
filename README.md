#  EasyReCaptcha  README #


### What is this repository for? ###

* Google ReCaptcha Wrapper for MVC.
* Current Stable Version 1.0.2


### How do I get set up? ###

* Install Nuget package
* Create account with Google ReCaptcha and obtain keys
* Add keys to the web.config
* Add ```@Html.ReCaptcha(null)``` to the page you want the recaptcha to show up
* Add ```[ReCaptchaValidation]``` validation tag to form post method and that is it!


### Who do I talk to? ###
