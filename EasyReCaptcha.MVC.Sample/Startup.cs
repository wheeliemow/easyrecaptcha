﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EasyReCaptcha.MVC.Sample.Startup))]

namespace EasyReCaptcha.MVC.Sample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}