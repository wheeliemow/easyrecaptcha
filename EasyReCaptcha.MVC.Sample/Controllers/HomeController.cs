﻿using System.Web.Mvc;

namespace EasyReCaptcha.MVC.Sample.Controllers
{
    public class HomeController : Controller
    {
        private string name;

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ReCaptchaValidation]
        public ActionResult Submit(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                name = form["name"];

                return RedirectToAction("Confirmed");
            }

            return View("Index");
        }

        public ActionResult Confirmed()
        {
            ViewBag.Name = name;

            return View();
        }
    }
}