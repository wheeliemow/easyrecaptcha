﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EasyReCaptcha.WebForms.Sample
{
    [ToolboxData("<{0}:MyTestControl runat=server></{0}:MyTestControl>")]
    public class MyTestControl : WebControl
    {
        [Bindable(true)]
        [DefaultValue("this is ttest code")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(Text);
        }
    }
}