﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EasyReCaptcha.WebForms.Sample._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>
    <p>test</p>
  
      <div class="g-recaptcha" data-sitekey="6LeqPSgTAAAAAIWYmuvJFeMuX5Bl6QySg6w-L4Op"></div>

		<%-- Browser default language will be used to display Recaptcha control--%>
		<script async="true" defer="true" src="https://www.google.com/recaptcha/api.js"></script>
    <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" />

    <p>test</p>
</asp:Content>