﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EasyReCaptcha.WebForms.Sample.Startup))]

namespace EasyReCaptcha.WebForms.Sample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}